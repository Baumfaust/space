#!/usr/bin/env python

import sys
import bodies
from math import sin, cos, sqrt, pi, radians

from PyQt5.QtWidgets  import QApplication, QMainWindow
from PyQt5.QtCore import Qt, QPoint, QPointF
from PyQt5.QtGui import QPainter, QColor, qRgb


__author__ = 'Baumfaust'

G_const = 6.67408E-11
623139223214.5416

class Orbital():
    def __init__(self, ecc, semi, inc, long, arg, ano):
        self.ecc = ecc
        self.semi = semi
        self.inc = inc
        self.long = long
        self.arg = arg
        self.ano_mean = ano

        self.ap = semi * (1 + ecc)
        self.pe = semi * (1 - ecc)
        self.minor_semi = sqrt((semi**2) - (semi*ecc)**2)


class Body():
    def __init__(self, name, mass, radius, orbital):
        self.name = name
        self.mass = mass
        self.radius = radius
        self.color = Qt.black
        self.orbital = orbital

        self.orbits = None
        self.orbited_by = []


def calc_ano_ecc(body):
    e = 0.0000001
    i = 0
    ano_ecc = body.orbital.ano_mean
    ano_ecc_i  = 0
    while(True):
        ano_ecc_i = body.orbital.ano_mean + body.orbital.ecc * sin(ano_ecc)
        if(ano_ecc_i - ano_ecc < e):
            break
        else:
            ano_ecc = ano_ecc_i
        i = i + 1
    return ano_ecc_i

class SpaceWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)

        self.star = bodies.load_kerbol_system()



        #calc_ano_ecc(body)

        self.greatest_ap = 0
        for ele in self.star.orbited_by:
            if ele.orbital.ap > self.greatest_ap:
                self.greatest_ap = ele.orbital.ap

    def rescale(self):
        size = self.size()
        ap = self.greatest_ap
        self.scale = float(1 / (ap / size.height()))
        self.scale = self.scale * 0.9


    def window_center(self):
        size = self.size()
        return QPoint(size.width()/2, size.height()/2)

    def paintOrbits(self):
        paint = QPainter()
        paint.begin(self)
        paint.translate(self.window_center().x(), self.window_center().y())

        for ele in self.star.orbited_by:
            paint.save()

            major = ele.orbital.semi * self.scale / 2.0
            minor = ele.orbital.minor_semi * self.scale / 2.0
            center = QPointF()
            center.setX(ele.orbital.semi * ele.orbital.ecc * self.scale / 2.0)
            center.setY(0)
            paint.rotate(-ele.orbital.arg)
            #paint.drawLine(0, 0, center.x(), center.y())
            paint.setPen(QColor(ele.color))
            paint.translate(center.x(), center.y())
            #paint.drawLine(center.x(), center.y(), center.x(), center.y()+50)

            paint.drawText(QPointF(center.x(), center.y() - minor - 2), ele.name)

            paint.drawEllipse(QPoint(0,0), major, minor)
            #if (ele.name == "Eeloo"):
                #paint.drawLine(0, 0, -major, 0)
            paint.drawLine(major, 0, major+10, 0)
            paint.drawLine(-major, 0, -major - 10, 0)
                #paint.drawLine(0, 0, 0, minor)


                #pe = ele.orbital.pe * self.scale / 2.0
                #paint.setPen(Qt.blue)
                #paint.drawLine(0, 0, 0, pe)



                #paint.drawLine(window_center.x(),window_center.y(), window_center.x()+pe*cos(radians(0)), window_center.y()-pe*sin(radians(0)))
                #paint.setPen(Qt.red)
                #paint.translate(window_center.x(),window_center.y())
                #paint.rotate(-45)
                #paint.drawLine( 0,0, window_center.x(), 0)
                #paint.drawLine(window_center.x(), window_center.y(), window_center.x() + pe * cos(radians(0)),
                 #              window_center.y() - pe * sin(radians(0)))
                #paint.drawLine(window_center.x(),window_center.y(), window_center.x()+pe*cos(radians(ele.orbital.arg)), window_center.y()-pe*sin(radians(ele.orbital.arg)))
            paint.restore()
        paint.end()

    def resizeEvent(self, event):
        self.rescale()

    def paintEvent(self, event):
        self.paintOrbits()
        paint = QPainter()
        paint.begin(self)
        window_center = self.window_center()

        paint.setBrush(Qt.yellow)
        paint.drawEllipse(window_center, 2, 2)

        paint.end()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    main_app = SpaceWindow()
    main_app.resize(999, 677)
    main_app.show()
    ret = app.exec_()
    sys.exit(ret)