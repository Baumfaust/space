from math import sqrt, pi
import datetime

G_const = 6.67408E-11




class CelestialBody:
    def __init__(self, mass, radius):
        self.mass = mass
        self.radius = radius

    def semi_major_axis(self, pe, ap):
        return (pe + self.radius + ap + self.radius) / 2

    def orbit_velocity(self, pe, ap):
        mha = self.semi_major_axis(pe, ap)
        vel_pe = sqrt(G_const * self.mass * ((2 / (pe+self.radius)) - (1 / mha)))
        vel_ap = sqrt(G_const * self.mass * ((2 / (ap+self.radius)) - (1 / mha)))
        return [vel_pe, vel_ap]

    def orbit_period(self, pe, ap):
        mha = self.semi_major_axis(pe, ap)
        return sqrt((4 * pi * pi * mha * mha * mha) / (G_const * self.mass))

if __name__ == '__main__':
    earth = CelestialBody(5.9722E+024, 6371000)
    #earth = CelestialBody(5972000000000000327155712, 6371000)
    2863.33
    # pe = 35786000
    # ap = 35786000
    ap = pe = 2863330
    vel_pe, vel_ap = earth.orbit_velocity(pe, ap)
    seconds = earth.orbit_period(pe, ap)
    print(vel_pe, vel_ap)
    print(str(datetime.timedelta(seconds=seconds)))
    print()

    kerbin = CelestialBody(5.2915793E+022, 600000)
    # kerbin = CelestialBody(5.291499E+022, 600000)
    vel_pe, vel_ap = kerbin.orbit_velocity(pe, ap)
    seconds = kerbin.orbit_period(pe, ap)
    print(vel_pe, vel_ap)
    print(str(datetime.timedelta(seconds=seconds)))
    print("21549.425")
    print(seconds)
    print()

    mars = CelestialBody(6.419E+023, 3396200)
    vel_pe, vel_ap = mars.orbit_velocity(pe, ap)
    seconds = mars.orbit_period(pe, ap)
    print(vel_pe, vel_ap)
    print(str(datetime.timedelta(seconds=seconds)))
    print()

    duna = CelestialBody(4.5154812E+021, 320000)
    vel_pe, vel_ap = duna.orbit_velocity(pe, ap)
    seconds = duna.orbit_period(pe, ap)
    print(vel_pe, vel_ap)
    print(str(datetime.timedelta(seconds=seconds)))
