#!/usr/bin/env python

import sys
from math import sin, cos, sqrt, pi, radians

from PyQt5.QtWidgets  import QApplication, QMainWindow
from PyQt5.QtCore import Qt, QPoint, QPointF
from PyQt5.QtGui import QPainter, QColor


__author__ = 'Baumfaust'

class Orbital():
    def __init__(self, ecc, semi, inc, long, arg, ano):
        self.ecc = ecc
        self.semi = semi
        self.inc = inc
        self.long = long
        self.arg = arg
        self.ano_mean = ano

        self.ap = semi * (1 + ecc)
        self.pe = semi * (1 - ecc)
        self.minor_semi = sqrt((semi**2) - (semi*ecc)**2)


class Body():
    def __init__(self, name, mass, radius, orbital, parent=None):
        self.name = name
        self.mass = mass
        self.radius = radius
        self.color = "000000"
        self.orbital = orbital

        self.orbits = parent
        self.orbited_by = []

def load_kerbol_system():
    kerbol_orbital = Orbital(0, 0, 0, 0, 0, 0)
    kerbol = Body("Kerbol", 1.7565459E+028, 261600000, kerbol_orbital)

    orbital = Orbital(0.2, 5263138304, 7.0, 70, 15, 3.14)
    body = Body("Moho", 2.5263314E+021, 250000, orbital, kerbol)
    body.color = "#D38D5F"
    kerbol.orbited_by.append(body)

    orbital = Orbital(0.01, 9832684544, 2.1, 15, 0, 3.14)
    body = Body("Eve", 1.2243980E+023, 700000, orbital, kerbol)
    body.color = "#8B5CD2"
    kerbol.orbited_by.append(body)

    orbital = Orbital(0, 13599840256, 0, 0, 0, 3.14)
    body = Body("Kerbin", 5.2915793E+022, 600000, orbital, kerbol)
    body.color = "#2A7FFF"
    kerbol.orbited_by.append(body)

    orbital = Orbital(0.05, 20726155264, 0.06, 135.5, 0, 3.14)
    body = Body("Duna", 4.5154270E+021, 320000, orbital, kerbol)
    body.color = "#FF4000"
    kerbol.orbited_by.append(body)

    orbital = Orbital(0.14, 40839348203, 5, 280, 90, 3.14)
    body = Body("Dres", 3.2190937E+020, 138000, orbital, kerbol)
    body.color = "#999999"
    kerbol.orbited_by.append(body)

    orbital = Orbital(0.05, 68773560320, 1.304, 52, 0, 0.1)
    body = Body("Jool", 4.2332127E+024, 6000000, orbital, kerbol)
    body.color = "#71C837"
    kerbol.orbited_by.append(body)

    orbital = Orbital(0.26, 90118820000, 6.15, 50, 260, 3.14)
    body = Body("Eeloo", 1.1149224E+021, 210000, orbital, kerbol)
    body.color = "#1A1A1A"
    kerbol.orbited_by.append(body)

    return kerbol